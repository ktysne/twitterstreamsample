//
//  ViewController.m
//  TwitterStreamSample
//
//  Created by @peace3884 on 2014/03/29.
//  Copyright (c) 2014年 @peace3884. All rights reserved.
//

#import "ViewController.h"

#import <Social/Social.h>
#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>

@interface ViewController () <UITextFieldDelegate, NSURLConnectionDataDelegate, NSURLConnectionDelegate>

@property (strong, nonatomic) NSArray *accounts;
@property (strong, nonatomic) NSURLConnection *connection;

@property (weak, nonatomic) IBOutlet UITextField *searchWordField;
@property (weak, nonatomic) IBOutlet UIButton *connectionButton;

@property (weak, nonatomic) IBOutlet UILabel *screenNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

- (IBAction)pushConnectionButton:(id)sender;
- (IBAction)enterSearchWordField:(id)sender;

- (void)openStream;
- (void)closeStream;

@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];

    ACAccountStore *accountStore = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    [accountStore requestAccessToAccountsWithType:accountType
                                          options:nil
                                       completion:^(BOOL granted, NSError *error) {
                                           
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                               if ( granted ) {
                                                   
                                                   NSLog(@"Twitterへのアクセス要求が認証された");
                                                   NSArray *accounts = [accountStore accountsWithAccountType:accountType];
                                                   [self setAccounts:accounts];
                                                   
                                                   if ( ![accounts count] ) {
                                                       
                                                       NSLog(@"アカウントが登録されていない");
                                                       [self.searchWordField setEnabled:NO];
                                                       [self.connectionButton setEnabled:NO];
                                                       
                                                   } else {
                                                       
                                                       [self.searchWordField becomeFirstResponder];
                                                   }
                                                   
                                               } else {
                                                   
                                                   NSLog(@"Twitterへのアクセス要求が拒否された");
                                                   [self.searchWordField setEnabled:NO];
                                                   [self.connectionButton setEnabled:NO];
                                               }
                                           });
                                       }
     ];
}

#pragma mark - IBAction
- (IBAction)pushConnectionButton:(id)sender {
    
    if ( self.connection ) {
     
        [self closeStream];
        
    } else {
        
        [self.searchWordField resignFirstResponder];
        [self openStream];
    }
}

- (IBAction)enterSearchWordField:(id)sender {
    
    [self openStream];
}

#pragma mark - Stream
- (void)openStream {
    
    if ( [self.searchWordField.text length] ) {
     
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            NSString *requestURL = @"https://stream.twitter.com/1.1/statuses/filter.json";
            NSDictionary *parameters = @{@"track" : self.searchWordField.text};
            SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                                    requestMethod:TWRequestMethodPOST
                                                              URL:[NSURL URLWithString:requestURL]
                                                       parameters:parameters];
            [request setAccount:self.accounts[0]];
            
            NSURLConnection *connection = [NSURLConnection connectionWithRequest:request.preparedURLRequest
                                                                        delegate:self];
            [self setConnection:connection];
            [connection start];
            
            dispatch_async(dispatch_get_main_queue(), ^{
               
                [self.connectionButton setTitle:@"接続中"
                                       forState:UIControlStateNormal];
                [self.connectionButton setEnabled:NO];
                [self.searchWordField setEnabled:NO];
            });
            
            while ( connection ) {
                
                [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode
                                         beforeDate:[NSDate distantFuture]];
            }
        });
        
    } else {
        
        NSLog(@"検索単語を入力してください");
        [self.searchWordField becomeFirstResponder];
    }
}

- (void)closeStream {
    
    [self.connection cancel];
    [self setConnection:nil];
    
    [self.connectionButton setTitle:@"接続"
                           forState:UIControlStateNormal];
    [self.connectionButton setEnabled:YES];
    [self.searchWordField setEnabled:YES];
    [self.screenNameLabel setText:@""];
    [self.textLabel setText:@""];
    [self.searchWordField becomeFirstResponder];
}

#pragma mark - NSURLConnectionDataDelegate
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {

    NSError *error = nil;
    id receiveData = [NSJSONSerialization JSONObjectWithData:data
                                                     options:NSJSONReadingMutableLeaves
                                                       error:&error];
    if ( !receiveData ) {
        
        return;
    }
    
    if ( error ) {
        
        NSLog(@"%@", error.localizedDescription);
        return;
    }
    
    NSLog(@"%@", receiveData);
    
    if ( [receiveData isKindOfClass:[NSDictionary class]] ) {
        
        NSDictionary *receiveTweet = receiveData;
        [self.screenNameLabel setText:receiveTweet[@"user"][@"screen_name"]];
        [self.textLabel setText:receiveTweet[@"text"]];
        
    } else if ( [receiveData isKindOfClass:[NSArray class]] ) {
        
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
    NSLog(@"didReceiveResponse:%ld, %lld", (long)httpResponse.statusCode, response.expectedContentLength);
    
    if ( httpResponse.statusCode == 200 ) {
        
        [self.connectionButton setTitle:@"切断"
                               forState:UIControlStateNormal];
        [self.connectionButton setEnabled:YES];
        
    } else {
        
        [self closeStream];
    }
}

#pragma mark - NSURLConnectionDelegate
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
    NSLog(@"%s", __func__);
    [self closeStream];
}

@end
