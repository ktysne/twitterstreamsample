//
//  main.m
//  TwitterStreamSample
//
//  Created by @peace3884 on 2014/03/29.
//  Copyright (c) 2014年 @peace3884. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
